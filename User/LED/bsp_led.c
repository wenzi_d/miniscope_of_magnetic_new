#include "bsp_led.h"

void LED_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	
	RCC_APB2PeriphClockCmd(LED1_PORT_CLK | LED2_PORT_CLK, ENABLE);
	
	GPIO_InitStruct.GPIO_Pin = LED1_PIN;
	GPIO_InitStruct.GPIO_Mode = LED1_Output_Mode;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	
	GPIO_Init(LED1_PORT,&GPIO_InitStruct);

	GPIO_InitStruct.GPIO_Pin = LED2_PIN;
	GPIO_InitStruct.GPIO_Mode = LED2_Output_Mode;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	
	GPIO_Init(LED1_PORT,&GPIO_InitStruct);
}



