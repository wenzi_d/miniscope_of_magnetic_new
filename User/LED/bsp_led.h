#ifndef _BSP_LED_H_
#define _BSP_LED_H_

#include "stm32f10x.h"

#define LED1_PORT_CLK  		RCC_APB2Periph_GPIOC
#define LED1_PIN  			GPIO_Pin_13 
#define LED1_Output_Mode 	GPIO_Mode_Out_PP
#define LED1_PORT			GPIOC

#define LED2_PORT_CLK  		RCC_APB2Periph_GPIOA
#define LED2_PIN  			GPIO_Pin_15 
#define LED2_Output_Mode 	GPIO_Mode_Out_PP
#define LED2_PORT			GPIOA 

void LED_Init(void);

#define LED1_OFF()     GPIO_SetBits(LED1_PORT, LED1_PIN)
#define LED1_ON()      GPIO_ResetBits(LED1_PORT, LED1_PIN)

#define LED2_OFF()     GPIO_SetBits(LED2_PORT, LED2_PIN)
#define LED2_ON()      GPIO_ResetBits(LED2_PORT, LED2_PIN)

#endif //_LED_H_
