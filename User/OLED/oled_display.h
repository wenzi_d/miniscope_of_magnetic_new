#ifndef __OLED_DISPLAY_H__
#define __OLED_DISPLAY_H__

#include <rtthread.h>

#define OLED_THREAD_PRIORITY    10
#define OLED_THREAD_TIMESLICE   10

#define  NPT  256

int OledTask(void);
void PlotChart(void);

int wavedata_init(void);
int display_init(void);

void Plot_Frequency_Chart(void);

#endif


