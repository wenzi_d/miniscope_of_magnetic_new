#include "chart.h"
#include <rtthread.h>
#include "OLED_I2C.h"

/** 
 * @brief  绘制 OLED 界面的线框
 * 		   包括界面实线和虚线的绘制
 * @author wenzi
 * @date   2020/7/20
*/
//void PlotChart(void)
//{
//	uint8_t i;
//	
//	OLED_DrawLine(20,8,20,56);    /* 绘制 y 轴 */

//	OLED_DrawPointN2(18,10);     /* 绘制 y 左箭头 */
//	OLED_DrawPointN2(19,9);
//	OLED_DrawPointN2(21,9);
//	OLED_DrawPointN2(22,10);

//	OLED_DrawLine(20,56,126,56);  /* 绘制 x 轴 */
//	
////	OLED_DrawPointN2(124,54);     /* 绘制 y 左箭头 */
////	OLED_DrawPointN2(125,55);
////	OLED_DrawPointN2(125,57);
////	OLED_DrawPointN2(124,58);
//	
//	OLED_Refresh();
//}