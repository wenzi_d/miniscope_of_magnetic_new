#include "bsp_led.h"
#include "bsp_usart.h"
#include "bsp_i2c_gpio.h"
#include "bsp_adc.h"
#include <math.h>
#include "bsp_init.h"
#include "beep.h"
#include <rtthread.h>
#include "TaskKey.h"
#include "oled_display.h"
#include "chart.h"
#include "oled.h"
#include "alarm.h"


#define accur 0.03544 

int main(void)
{
	uint8_t x;
	
	uint16_t outvalue;
	int16_t b_value;
	char buffer[5];
	float out_vol;
	float b_vol;

	OLED_Clear();
	OLED_Set_Pos(8, 56);
	OLED_DrawString("A");
	OLED_Display();
	bsp_init();

	POWER_ON();
	SENSOR_ON();

	LED1_ON();
	LED2_ON();
	
	PlotChart();
	OLED_Display();
	
	BEEP_ON();
	rt_thread_delay(100);
	BEEP_OFF();

	key_task();
	wavedata_init();
	
	alarm_Task();
	
	display_init();
	
	while(1)
	{
		rt_thread_delay(10);
	}
}
