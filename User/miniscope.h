#ifndef __MINISCOPE_H__
#define __MINISCOPE_H__

#include <rtthread.h>
#include "menu.h"

#define ADC_CONVERT_PERIOD_US   1   /* 1M ADC Convert Rate */

#define ADC_MAX_VOLT    3300
#define ADC_MIN_VOLT    0

#define ADC_SAMPLE_NUM          210
#define WAVE_DATA_NUM           210

#define    NPT   256

#define SCALE_TO_INTERVAL(s)    ((s)*4/ADC_SAMPLE_NUM)

/* thread info */
#define ADC_THREAD_PRIO             5
#define ADC_THREAD_STACK_SIZE       256
#define PARSE_THREAD_PRIO           6
#define PARSE_THREAD_STACK_SIZE     256
#define DIS_THREAD_PRIO             4
#define DIS_THREAD_STACK_SIZE       512
#define MENU_THREAD_PRIO            3
#define MENU_THREAD_STACK_SIZE      256

struct Adc_Info
{
    rt_uint32_t channel;
    rt_int16_t *buff;
    rt_mailbox_t mb;
	rt_sem_t adc_complete_sem;
    rt_uint32_t interval_us;
	float  Vout;
	float  B_Value;
};

struct Wave_Info
{
    rt_int16_t *data;
    rt_mailbox_t mb;
    rt_int16_t vMax;       /* mv */
    rt_int16_t vMin;
    rt_int16_t rulerVMax;
    rt_int16_t rulerVMin;

    float wave_frequency;
};

struct Freqency_Info
{
	rt_uint32_t lBufInArray[NPT];
	rt_uint32_t lBufOutArray[NPT/2];
	rt_uint32_t lBufMagArray[NPT/2];
	rt_uint16_t fre_wave[NPT/2];
	
	rt_uint16_t sample_arr;
	rt_uint16_t sample_psc;

    rt_uint16_t frequency_max;
    rt_uint16_t frequency_min;
    rt_uint16_t frequency_max_position;
	
	rt_mailbox_t mb;
};

struct Miniscope 
{
    struct Adc_Info adc;
    struct Wave_Info wave;
    struct Menu_Info menu[MENU_TYPE_MAX_NUM];
	struct Freqency_Info freqency;

    rt_uint16_t option_index;
    rt_uint16_t tri_pos;
    rt_bool_t trig_dire;
    rt_err_t trigger_state;
    rt_event_t key_event;
};

int miniscope_init(void);

extern struct Miniscope miniscope;

#endif /* #ifndef __MINISCOPE_H__ */
