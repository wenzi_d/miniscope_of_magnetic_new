#include "bsp_init.h"
#include "bsp_led.h"
#include "bsp_adc.h"
#include "beep.h"
#include "oled.h"
#include "miniscope.h"
#include <rtthread.h>


/**
  * @brief  传感器 IO 初始化
  * @author wenzi
  * @param  无
  * @retval 无
  * @date   2020/7/27
*/
static void sensor_gpio_init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	
	RCC_APB2PeriphClockCmd(SENSOR_ERR_CLK | SENSOR_OR_CLK, ENABLE);
	
	GPIO_InitStruct.GPIO_Pin = SENSOR_ERR_PIN;
	GPIO_InitStruct.GPIO_Mode = SENSOR_ERR_Input_Mode;
	
	GPIO_Init(SENSOR_ERR_PORT,&GPIO_InitStruct);

	GPIO_InitStruct.GPIO_Pin = SENSOR_OR_PIN;
	GPIO_InitStruct.GPIO_Mode = SENSOR_OR_Input_Mode;
	
	GPIO_Init(SENSOR_OR_PORT,&GPIO_InitStruct);
}


/**
  * @brief  总开关 IO 和传感器开关初始化
  * @author wenzi
  * @param  无
  * @retval 无
*/
static void power_gpio_init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	
	RCC_APB2PeriphClockCmd(POWER_PORT_CLK | SENSOR_PORT_CLK, ENABLE);
	
	GPIO_InitStruct.GPIO_Pin = POWER_PIN;
	GPIO_InitStruct.GPIO_Mode = POWER_Output_Mode;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	
	GPIO_Init(POWER_PORT,&GPIO_InitStruct);

	GPIO_InitStruct.GPIO_Pin = SENSOR_PIN;
	GPIO_InitStruct.GPIO_Mode = SENSOR_Output_Mode;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	
	GPIO_Init(SENSOR_PORT,&GPIO_InitStruct);
}


/**
  * @brief  板级初始化
  * @author wenzi
  * @param  无
  * @retval 无
*/
void bsp_init(void)
{
  LED_Init();
  OLED_Init();
  miniscope_init();
  ADC_Configuration();
  TIM_Cmd(TIM4, ENABLE);
  power_gpio_init();
  beep_gpio();
  sensor_gpio_init();
}
