#ifndef __BSP_INIT_H__
#define __BSP_INIT_H__
#include "stm32f10x.h"

#define POWER_PORT_CLK  		RCC_APB2Periph_GPIOA
#define POWER_PIN  			    GPIO_Pin_7 
#define POWER_Output_Mode 	    GPIO_Mode_Out_PP
#define POWER_PORT			    GPIOA

#define SENSOR_PORT_CLK  		RCC_APB2Periph_GPIOA
#define SENSOR_PIN  			GPIO_Pin_6 
#define SENSOR_Output_Mode 	    GPIO_Mode_Out_PP
#define SENSOR_PORT			    GPIOA

#define SENSOR_ERR_CLK          RCC_APB2Periph_GPIOA     
#define SENSOR_ERR_PORT         GPIOA
#define SENSOR_ERR_PIN          GPIO_Pin_3
#define SENSOR_ERR_Input_Mode   GPIO_Mode_IN_FLOATING

#define SENSOR_OR_CLK           RCC_APB2Periph_GPIOA 
#define SENSOR_OR_PORT          GPIOA
#define SENSOR_OR_PIN           GPIO_Pin_4
#define SENSOR_OR_Input_Mode    GPIO_Mode_IN_FLOATING

#define POWER_ON()      GPIO_SetBits(POWER_PORT, POWER_PIN)
#define POWER_OFF()     GPIO_ResetBits(POWER_PORT, POWER_PIN)

#define SENSOR_ON()      GPIO_SetBits(SENSOR_PORT, SENSOR_PIN)
#define SENSOR_OFF()     GPIO_ResetBits(SENSOR_PORT, SENSOR_PIN)

void bsp_init(void);

#endif

