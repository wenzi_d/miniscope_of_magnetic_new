#ifndef __ADC_H
#define	__ADC_H


#include "stm32f10x.h"
void ADC_Configuration(void);

void ADC1_DMA1_IT_Hander(void);

#define ADC_BUFF_LEN	256

#endif /* __ADC_H */

