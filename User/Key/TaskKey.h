#ifndef __TASKKEY_H__
#define __TASKKEY_H__

#include <rtthread.h>

int key_task(void);

#define    KEY_THREAD_PRIORITY    1
#define    KEY_THREAD_TIMESLICE   10

#endif


