/************************************************************************
*
* 文件名：TaskKey.c
*
* 文件描述：按键任务处理函数
*
* 创建人：wenzi, 2020年 09 月 09 日
*
* 版本号：V1.0
*
* 修改记录：
*
************************************************************************/

#include "TaskKey.h"
#include "bsp_key.h"
#include "beep.h"
#include "bsp_init.h"
#include "miniscope.h"
#include "stm32f10x_tim.h"


ALIGN(RT_ALIGN_SIZE)
static char key_thread_stack[1024];
static struct rt_thread key_thread;
rt_uint8_t switch_count = 0;
extern rt_thread_t adc_thread;
rt_uint8_t frequency_flag = 0;

/*================================================================
*
* 函 数 名：key_thread_entry
*
* 参 数：
*
* void* param: 线程入口参数
*
* 功能描述:
*
* 按键的处理函数
*
* 返 回 值：void
*
* 抛出异常：
*
* 作 者：wenzi 2020 年 09 月 09 日
*
================================================================*/
static void key_thread_entry(void *param)
{
	uint8_t ucKeyCode;
	static rt_uint8_t hold_count = 0;
	
	while(1)
	{
		ucKeyCode = bsp_GetKey();
		if (ucKeyCode > 0)
		{
			/* 有键按下 */
			switch (ucKeyCode)
			{			
				case KEY_3_DOWN:
					switch_count++;        /* 单位切换 */
					break;
				
				case KEY_3_LONG:
					switch_count = 255;
					break;
									
				case KEY_5_OK_DOWN:
					frequency_flag++;          /* 切换至频谱图显示 */
					break;

				case KEY_1_DOWN:
					hold_count++;
					break;
				
				case KEY_1_LONG:               /* 关机 */
					BEEP_ON();
					rt_thread_delay(100);
					BEEP_OFF();
					POWER_OFF();	
					break;
				
				
				case KEY_6_UP_DOWN:
					miniscope.wave.rulerVMax += 50;  /* 增大纵坐标量程 */
					miniscope.wave.rulerVMin -= 50;	 
					break;
				
				case KEY_8_DOWN_DOWN:
					miniscope.wave.rulerVMax -= 50;  /* 减小纵坐标量程 */
					miniscope.wave.rulerVMin += 50;	
					if (miniscope.wave.rulerVMax <= 0)
					{
						miniscope.wave.rulerVMax = 50;
						miniscope.wave.rulerVMin = -50;
					}
					break;

				case KEY_7_RIGHT_DOWN:
					miniscope.freqency.sample_psc -=50;
					if (miniscope.freqency.sample_psc < 0)
						miniscope.freqency.sample_psc = 1;
					TIM_PrescalerConfig(TIM4,miniscope.freqency.sample_psc,TIM_PSCReloadMode_Immediate); 
					break;
					
				case KEY_4_L_DOWM:
					miniscope.freqency.sample_psc +=50;
					if (miniscope.freqency.sample_psc > 65500)
						miniscope.freqency.sample_psc = 65500;
					TIM_PrescalerConfig(TIM4,miniscope.freqency.sample_psc,TIM_PSCReloadMode_Immediate); 					
					break;

				default:
					break;
			}
			if (hold_count % 2 == 0)
			{
				rt_thread_resume(adc_thread);
			}
			else
			{
				rt_thread_suspend(adc_thread);
			}
		}
		
		rt_thread_delay(10);
	}
}

/*================================================================
*
* 函 数 名：XXX
*
* 参 数：
*
* type name [IN] : descripts
*
* 功能描述:
*
* ..............
*
* 返 回 值：成功TRUE，失败FALSE
*
* 抛出异常：
*
* 作 者：ChenHao 20XX/4/2
*
================================================================*/
int key_task(void)
{
	rt_thread_init(&key_thread,
					"key_thread",
					key_thread_entry,
					RT_NULL,
					&key_thread_stack[0],
					sizeof(key_thread_stack),
					KEY_THREAD_PRIORITY,
                    KEY_THREAD_TIMESLICE);

	rt_thread_startup(&key_thread);
	
	return 0;
}