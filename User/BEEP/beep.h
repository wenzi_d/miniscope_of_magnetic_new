#ifndef __BEEP_H__
#define __BEEP_H__

#include "stm32f10x.h"

#define BEEP_PORT_CLK  		RCC_APB2Periph_GPIOA
#define BEEP_PIN  			GPIO_Pin_8 
#define BEEP_Output_Mode 	GPIO_Mode_Out_PP
#define BEEP_PORT			GPIOA


#define BEEP_ON()       GPIO_SetBits(BEEP_PORT, BEEP_PIN)
#define BEEP_OFF()      GPIO_ResetBits(BEEP_PORT, BEEP_PIN)

void beep_gpio(void);

#endif

