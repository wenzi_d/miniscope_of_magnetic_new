#include "beep.h"


/**
  * @brief  蜂鸣器对应的 IO 口初始化
  * @author wenzi
  * @param  无
  * @retval 无
*/
void beep_gpio(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	
	RCC_APB2PeriphClockCmd(BEEP_PORT_CLK, ENABLE);
	
	GPIO_InitStruct.GPIO_Pin = BEEP_PIN;
	GPIO_InitStruct.GPIO_Mode = BEEP_Output_Mode;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	
	GPIO_Init(BEEP_PORT,&GPIO_InitStruct);
}